<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Data extends Model
{
    use HasFactory;

    protected $table = 'data';

    protected $fillable = [
        'key1',
        'key2',
        'value'
    ];

    /**
     * @param int $relevance
     *
     * @return mixed
     */
    public function getListRelevanceForDelete(int $relevance)
    {
        return self::select('key2')
            ->where('created_at', '<=', Carbon::now()->subDays($relevance)->toDateTimeString())
            ->limit(5)
            ->get();
    }

    /**
     * @param null $period
     *
     * @return mixed
     */
    public function deleteForPeriod($period = null)
    {
        if ($period === null) {
            $period = Carbon::now()->startOfMonth()->subMonth()->toDateTimeString();
        } else {
            $period = Carbon::now()->subDays($period)->toDateTimeString();
        }

        return self::select('key2')
            ->where('created_at', '<=', $period)
            ->delete();
    }
}
