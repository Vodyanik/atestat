<?php

namespace App\Console\Commands;

use App\Models\Data;
use Illuminate\Console\Command;

class DataDelete extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'data:delete {--question} {--cron}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Data delete';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     * @throws \Exception
     */
    public function handle()
    {
        if ($this->option('cron')) {
            (new Data())->deleteForPeriod();
        } else {
            $relevance = (int)$this->ask('Set count days of relevance (records older than this period will be deleted): ');
            $data      = (new Data())->getListRelevanceForDelete($relevance);

            if ($data->isNotEmpty()) {
                $this->line('Data to be deleted: ');
                foreach ($data as $d) {
                    $this->line($d->key2);
                }
            } else {
                $this->warn('Data is empty');
                return;
            }

            $confirm = true;
            if (!$this->option('question')) {
                $confirm = $this->confirm('Do you wish to delete data?');
            }

            if ($confirm) {
                $this->newLine();
                $timer = random_int(5, 10);
                $bar   = $this->output->createProgressBar($timer);

                (new Data())->deleteForPeriod($relevance);
                for ($i = $timer; $i > 0; $i--) {
                    $bar->advance();
                    sleep(1);
                }

                $bar->finish();
            }
            $this->newLine(2);
        }
    }
}
