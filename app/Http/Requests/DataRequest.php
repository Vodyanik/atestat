<?php

namespace App\Http\Requests;

use App\Rules\PhoneNumber;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

abstract class DataRequest extends FormRequest
{
    protected function getKey2ValidationType(Request $request)
    {
        switch ($request->key1) {
            case 'email':
                return 'email';
            case 'phone':
                return new PhoneNumber();
        }
    }
}
