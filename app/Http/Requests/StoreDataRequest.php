<?php

namespace App\Http\Requests;

use Illuminate\Http\Request;

class StoreDataRequest extends DataRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @param Request $request
     * @return array
     */
    public function rules(Request $request)
    {
        return [
            'key1'  => [
                'required',
                'in:phone,email',
                'max:20',
            ],
            'key2'  => [
                'required',
                'unique:data',
                'max:32',
                $this->getKey2ValidationType($request),
            ],
            'value' => [
                'required',
                'json',
            ],
        ];
    }
}
