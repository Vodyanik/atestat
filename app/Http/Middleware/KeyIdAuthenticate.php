<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class KeyIdAuthenticate
{
    private $keyIdHard = [
        'p8EzFznvB1fuzNmLT6q6ptQiu2jEu0PQ' => 'bgqR1PEGG9',
        'qYbzkSFiNtNmQa6EtkiOhLPcLZzx25I4' => 'oXOxpjtI9N',
    ];

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (isset($this->keyIdHard[$request->get('api_key')])) {
            if ($this->keyIdHard[$request->get('api_key')] === $request->get('api_id')) {
                return $next($request);
            }
        }

        return response()->json([
            'message' => 'Unauthorized',
        ], 401);
    }
}
