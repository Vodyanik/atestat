<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\SearchDataRequest;
use App\Http\Requests\StoreDataRequest;
use App\Http\Resources\SearchDataCollection;
use App\Models\Data;

class DataController extends Controller
{
    /**
     * @param SearchDataRequest $request
     *
     * @return SearchDataCollection
     */
    public function search(SearchDataRequest $request)
    {
        $data = Data::select(['key1', 'key2', 'value'])
            ->where('key1', $request->get('key1'))
            ->where('key2', $request->get('key2'))
            ->get();

        return new SearchDataCollection($data);
    }

    /**
     * @param StoreDataRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreDataRequest $request)
    {
        $post = $request->post();

        Data::create([
            'key1' => $post['key1'],
            'key2' => $post['key2'],
            'value' => $post['value'],
        ]);

        return response()->json([
            'message' => 'Successful added',
        ]);

    }
}
